#!/usr/bin/env python
#################################################
#This file computes the leaks from DY outside the signal region
#################################################
from __future__ import division
from array import array
import ROOT
from ROOT import *
TH2.SetDefaultSumw2()
import math
from math import sqrt
import os

infile = "/eos/atlas/atlascerngroupdisk/phys-exotics/ueh/VH4b/MININTUP/ZH_Signal.2020-09-17.1.0/merged_ntuples/ZH_H125_a15a15_4b_ctau100.root"

def find_max(h_list):
    old_max = None
    old_min = None
    for h in h_list:
        max = h.GetBinContent(h.GetMaximumBin()) #Find max value in Y for sample
        #Set new max and min values for scaling of Y axis
        if old_max is None:
            old_max = max
        elif (old_max < max):
            old_max = max
    return old_max
def draw_tex(name):
    ATLASLabel(0.2, 0.87,"Internal")

    # Lumi / fb
    t2 = TLatex()
    t2.SetNDC(kTRUE)
    t2.SetTextFont(42)
    t2.SetTextSize(0.028)
    t2.DrawLatex(0.2,.83,"#sqrt{s} = 13 TeV, 139 fb^{-1}")

    # Semilep
    t0 = TLatex()
    t0.SetNDC(kTRUE)
    t0.SetTextFont(42)
    t0.SetTextSize(0.028)
    t0.DrawLatex(0.2,.79,"HDMI")

    t1 = TLatex()
    t1.SetNDC(kTRUE)
    t1.SetTextFont(42)
    t1.SetTextSize(0.028)
    t1.DrawLatex(0.275,.79,name)

def dual_hists(h1,h2):
    can  = TCanvas("can", "can", 0, 0, 600, 600)
    pad1 = TPad("pad1","pad1",0,0.3,1,1)
    pad2 = TPad("pad2","pad2",0,0,1,0.29)
    pad1.SetBottomMargin(0.025)
    pad2.SetTopMargin(0.04)
    pad2.SetBottomMargin(0.4)

    pad1.Draw()
    pad2.Draw()
    pad1.cd()
    ymax = find_max([h1,h2])
    # if (h1.GetName() == 'h_minjetDR'):
    pad1.SetLogy()
    h1.SetMaximum(20*ymax)
    # else:
    #     h1.SetMaximum(1.4*ymax)
    h1.Draw('HIST')
    h2.Draw('SAME HIST')
    h1.SetMarkerColor(1)
    h2.SetMarkerColor(3)
    h1.SetLineColor(1)
    h2.SetLineColor(3)
    leg= TLegend(0.7, 0.73, 0.85, 0.9)
    leg.AddEntry(h1, h1.GetName(), "l")
    leg.AddEntry(h2, h2.GetName(), "l")
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetTextSize(0.04)
    leg.SetTextFont(42)
    leg.Draw()

    draw_tex(name)

    can.cd()
    pad2.cd()
    pad2.SetGridy()

    # Abs(weight_mc) + axis setup
    h2r = h1.Clone("h2r")
    h2r.SetStats(0)
    h2r.Divide(h2r,h2,1,1)
    h2r.SetMaximum(1.4*h2r.GetMaximum())
    if (h2r.GetMinimum() != 0):
        h2r.SetMinimum(0.6*h2r.GetMinimum())
    else:
        h2r.SetMinimum(0)
    # h2r.GetXaxis().SetRangeUser(xmin,xmax)

    h2r.GetXaxis().SetTitleSize(.16)
    h2r.GetXaxis().SetTitleOffset(1.1)
    h2r.GetXaxis().SetLabelSize(0.125)
    h2r.GetXaxis().SetLabelOffset(0.03)
    h2r.GetXaxis().SetTitle(h1.GetName())

    h2r.GetYaxis().SetNdivisions(406,1)
    h2r.GetYaxis().SetTitleSize(0.13)
    h2r.GetYaxis().SetLabelSize(.1)
    h2r.GetYaxis().SetTitleOffset(0.45)
    h2r.GetYaxis().SetTitle("Ratio")
    h2r.SetFillColorAlpha(h1.GetMarkerColor(), 0.5)
    h2r.Draw("PE3")

    line = TF1("line", "1", -1e5, 1e5)
    line.SetLineColor(51)
    line.SetLineWidth(1)
    line.Draw("SAME")

    can.cd()
    can.Update()
    can.SaveAs(outDir+"/jetmedium_"+h2.GetName()+'_'+name+".png")
    can.SaveAs(outDir+"/jetmedium_"+h2.GetName()+'_'+name+".pdf")
    del can
def drawjet(rapidity,phi,col,first = False):
    circle = ROOT.TEllipse(rapidity, phi, 0.4, 0, 0, 360, 0)
    circle.SetFillColorAlpha(col,0.25)
    # circle.DrawEllipse(rapidity, phi, 0.4, 0, 0, 360, 0)
    if (not first):
        circle.Draw("SAME")
    else:
        circle.Draw()
def drawleg(x,y,name):
    t0 = TLatex()
    t0.SetNDC(kTRUE)
    t0.SetTextFont(42)
    t0.SetTextSize(0.028)
    t0.DrawLatex(x,y,name)
def drawmarker(x,y,m,col):
    marker = ROOT.TMarker()
    marker.SetNDC(kTRUE)
    marker.SetMarkerColorAlpha(col,0.5)
    marker.SetMarkerSize(1.5)
    marker.SetMarkerStyle(m)
    marker.DrawMarker(x, y)

def ToArray(infile, fname, output):
    rootFile = ROOT.TFile(infile)
    tree = rootFile.Get('treeZ')
    treeName = str(tree.GetName())
    global name
    name = fname
    print treeName

    centroid_array = array( 'd' )
    ev_array = array( 'd' )
    outputhists = []

    fk = ROOT.TFile(str(output)+".root","UPDATE")
    h_new_centroid = ROOT.TH1F("h_new_centroid", "new centroid; CentroidR", 200, 0, 5000)
    h_jet_phi = ROOT.TH1F("h_jet_phi", "jet_phi; jet_phi", 50, 0, 5)
    h_jet_eta = ROOT.TH1F("h_jet_eta", "jet_eta; jet_eta", 50, 0, 5)
    h_minjetDR = ROOT.TH1F("h_minjetDR", "minjetDR; minjetDR", 100, 0, 10)
    h_uncor_centroid = ROOT.TH1F("h_uncor_centroid", "jet_CentroidR", 200, 0, 5000)
    h_jet_phi_uncor = ROOT.TH1F("h_jet_phi_uncor", "jet_phi; jet_phi", 50, 0, 5)
    h_jet_eta_uncor = ROOT.TH1F("h_jet_eta_uncor", "jet_eta; jet_eta", 50, 0, 5)
    h_minjetDR_uncor = ROOT.TH1F("h_minjetDR_uncor", "minjetDR; minjetDR", 100, 0, 1)
    h_jet_dphi = ROOT.TH1F("h_jet_dphi", "jet_dphi; jet_dphi", 50, 0, .8)
    h_jet_diffdphi = ROOT.TH1F("h_jet_diffdphi", "jet_dphi; jet_dphi", 50, 0, .8)
    h_jet_deta = ROOT.TH1F("h_jet_deta", "jet_deta; jet_deta", 50, 0, .8)
    h_jet_diffdeta = ROOT.TH1F("h_jet_diffdeta", "jet_deta; jet_deta", 50, 0, .8)
    h_dminjetDR = ROOT.TH1F("h_dminjetDR", "dminjetDR; dminjetDR", 40, -0.2, 0.6)
    h_truedminjetDR = ROOT.TH1F("h_truedminjetDR", "truedminjetDR; dminjetDR", 40, -0.2, 0.6)
    h_diffdminjetDR = ROOT.TH1F("h_diffdminjetDR", "diffdminjetDR; dminjetDR", 40, -0.2, 0.6)
    h_jetDR = ROOT.TH1F("h_jetDR", "jetDR; jetDR", 20, 0, 0.6)
    h_truejetDR = ROOT.TH1F("h_truejetDR_"+name, "truejetDR; jetDR", 30, 0, 0.3)
    h_aaDR = ROOT.TH1F("h_aaDR", "aaDR; aaDR", 60, 0, 10)
    h_trueaaDR = ROOT.TH1F("h_trueaaDR_"+name, "aaDR; aaDR", 60, 0, 10)
    h_dpt = ROOT.TH1F("h_dpt", "dpt; dpt", 100, -10, 10)
    h_ind = ROOT.TH1F("h_ind", "improvement; improvement", 2, 0, 1)
    h_lxy = ROOT.TH1F("h_lxy", "Lxy; Lxy", 200, 0, 1000)
    h_angular_event = ROOT.TH2F("h_angular_event", "Angular Distribution", 1080, -5.4, 5.4,700,-3.5,4.5)
    h_dv_event = ROOT.TH2F("h_dv_event", "Angular Distribution", 1080, -5.4, 5.4,700,-3.5,4.5)
    h_pvdv_event = ROOT.TH2F("h_pvdv_event", "Angular Distribution", 1080, -5.4, 5.4,700,-3.5,4.5)
    h_jet_event = ROOT.TH2F("h_jet_event", "Angular Distribution", 1080, -5.4, 5.4,700,-3.5,4.5)
    h_jet_cor_event = ROOT.TH2F("h_jet_cor_event", "Angular Distribution", 1080, -5.4, 5.4,700,-3.5,4.5)
    h_bjet_event = ROOT.TH2F("h_bjet_event", "Angular Distribution", 1080, -5.4, 5.4,700,-3.5,4.5)
    h_b_event = ROOT.TH2F("h_b_event", "Angular Distribution", 1080, -5.4, 5.4,700,-3.5,4.5)
    improved = 0
    dropped = 0
    tlen = 0
    neg_dR = 0
    dv_matched = 0
    total_truths = 0
    jet_matched = 0
    total_truth_jets = 0
    gROOT.SetBatch(True)
    atlasrootstyle_path = '/afs/cern.ch/work/d/dstarko/WorkArea/.local/opt/atlasrootstyle'
    gROOT.SetMacroPath(atlasrootstyle_path)
    gROOT.LoadMacro("AtlasStyle.C")
    gROOT.LoadMacro("AtlasLabels.C")
    TH1.SetDefaultSumw2(True)
    SetAtlasStyle()
    can  = TCanvas("can", "can", 0, 0, 600, 600)
    leg= TLegend(0.75, 0.75, 0.9, 0.9)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetTextSize(0.035)
    leg.SetTextFont(42)
    leg.Draw()
    can.Draw()
    can.Print(outDir+"/jetmedium_"+'h_angular_event_'+name+"_plots.pdf[")
    ind = 0
    events = [10438,10423,11612,5503160,5503085,5504596,5503610]
    for i, ev in enumerate(tree):
        # if i > 100:
        #     break
        if (ev.eventNumber not in events):# 8828
            continue
        else:
            evn = ev.eventNumber
            ind+=1
            print "Found event "+str(evn)+"!" #9494 10438 10423 11612
        tlen +=1
        minJetDR = 99
        minJetDR_uncor = 99

        truth_l = 0
        pvx = ev.PV_x
        pvy = ev.PV_y
        pvz = ev.PV_z
        PV = TVector3(pvx,pvy,pvz)
        # print 'Event Number:\t'+str(ev.eventNumber)
        first_print = True
        b_lines = []
        truth_aa = True
        for l,truth_DV_x in enumerate(ev.truthVtx_x):
            truth_DV_y = ev.truthVtx_y.at(l)
            truth_DV_z = ev.truthVtx_z.at(l)
            truth_DV = TVector3(truth_DV_x,truth_DV_y,truth_DV_z)
            if (ev.truthVtx_parent_pdgId.at(l) != 36 and truth_aa == True):
                truth_aa = False
            print "truth vertex index %i position (x,y,z,pdgID,pt):\t(%.1f,%.1f,%.1f,%i,%.2f)" % (ev.truthVtx_ID.at(l),truth_DV_x,truth_DV_y,truth_DV_z,ev.truthVtx_parent_pdgId.at(l),ev.truthVtx_parent_pt.at(l))
            print 'truth vertex (eta,phi):\t\t\t\t\t(%.2f,%.2f)' % (ev.truthVtx_eta.at(l),ev.truthVtx_phi.at(l))
            b_lines.append([])
            for z,outp_id in enumerate(ev.truthVtx_outP_pdgId.at(l)):
                truth_b = TLorentzVector()
                truth_b.SetPtEtaPhiM(ev.truthVtx_outP_pt.at(l).at(z),ev.truthVtx_outP_eta.at(l).at(z),ev.truthVtx_outP_phi.at(l).at(z),ev.truthVtx_outP_M.at(l).at(z))
                h_b_event.Fill(truth_b.Rapidity(),ev.truthVtx_outP_phi.at(l).at(z))
                b_lines[l].append([truth_b.Rapidity(),ev.truthVtx_outP_phi.at(l).at(z)])
                print "output particle pdgID and  (pt,y,phi,m):\t%i\t(%.2f,%.2f,%.2f,%.2f)" % (outp_id,ev.truthVtx_outP_pt.at(l).at(z),truth_b.Rapidity(),ev.truthVtx_outP_phi.at(l).at(z),ev.truthVtx_outP_M.at(l).at(z))
            truth_l += 1
            total_truths+=1
        for l,truth_LLP_pt in enumerate(ev.truth_LLP_pt):
            truth_LLP_eta = ev.truth_LLP_eta.at(l)
            truth_LLP_phi = ev.truth_LLP_phi.at(l)
            truth_LLP_m = ev.truth_LLP_m.at(l)
            truth_LLP = TLorentzVector()
            truth_LLP.SetPtEtaPhiM(truth_LLP_pt,truth_LLP_eta,truth_LLP_phi,truth_LLP_m)
            h_angular_event.Fill(truth_LLP.Rapidity(),truth_LLP_phi)
            print "truth LLP (x,y,z):\t\t\t\t\t(%.1f,%.1f,%.1f)" % (ev.truth_LLP_x.at(l),ev.truth_LLP_y.at(l),ev.truth_LLP_z.at(l))
            print "truth LLP (pt,y,phi,m):\t\t\t\t\t(%.1f,%.1f,%.1f,%.1f)" % (truth_LLP_pt,truth_LLP.Rapidity(),truth_LLP_phi,truth_LLP_m)
        # matched_jet_lists = {}
        # print "number of truths: "+str(truth_l)
        # print "truth parent ID %i and mass %.1f" % (ev.truthVtx_parent_pdgId.at(l),ev.truthVtx_parent_M.at(l))
        drawjets = False
        # for j, DV_x in enumerate(ev.truth_LLP_x):
        for j, DV_x in enumerate(ev.secVtxMedium_x):
            if (ev.secVtxMedium_ntrk.at(j) <= 2):
                continue
            DV_y = ev.secVtxMedium_y.at(j)
            DV_z = ev.secVtxMedium_z.at(j)
            # DV_y = ev.truth_LLP_y.at(j)
            # DV_z = ev.truth_LLP_z.at(j)
            DV_3D = TVector3(DV_x,DV_y,DV_z)
            DV = TLorentzVector()
            DV.SetPtEtaPhiM(ev.secVtxMedium_pt.at(j),ev.secVtxMedium_eta.at(j),ev.secVtxMedium_phi.at(j),ev.secVtxMedium_mass.at(j))
            # DV.SetPtEtaPhiM(ev.truth_LLP_pt.at(j),ev.truth_LLP_eta.at(j),ev.truth_LLP_phi.at(j),ev.truth_LLP_m.at(j))
            minJetDR_old = 99
            minJetDR = 99
            minJetDR_uncor = 99
            minJetDR_temp = 99
            h_dv_event.Fill(DV.Rapidity(),ev.secVtxMedium_phi.at(j))
            h_pvdv_event.Fill((DV_3D-PV).Eta(),(DV_3D-PV).Phi())
            print '%.2f,%.2f' % ((DV_3D-PV).Eta(),(DV_3D-PV).Phi())
            # print '\tDV Index:\t\t'+str(j)
            # print '\tDV (x,y,z,pdgID)\t\t (%.1f,%.1f,%.1f,%i)' % (DV_x,DV_y,DV_z,ev.secVtxMedium_maxlinkTruth_parent_pdgId.at(j))
            # print '\tDV (pt,y,eta,phi,m)\t\t (%.2f,%.2f,%.2f,%.2f,%.2f)' % (ev.secVtxMedium_pt.at(j),DV.Rapidity(),DV.Eta(),DV.Phi(),DV.M())
            # print "\tDV_3D (eta,phi):\t\t(%.2f,%.2f)" % (DV_3D.Eta(),DV_3D.Phi())
            # print "\tDV_3D-PV (eta,phi):\t\t(%.2f,%.2f)" % ((DV_3D-PV).Eta(),(DV_3D-PV).Phi())
            for k, C_R in enumerate(ev.jet_CentroidR):
                if (ev.jet_pt.at(k) < 20 or abs(ev.jet_eta.at(k)) > 2.5):
                    continue
                if (ev.jet_HadronConeExclTruthLabelID.at(k) == 5):
                    total_truth_jets += 1
                C_T = C_R/cosh(ev.jet_eta.at(k))
                C_x = C_T*cos(ev.jet_phi.at(k))
                C_y = C_T*sin(ev.jet_phi.at(k))
                C_z = C_T*sinh(ev.jet_eta.at(k))
                jet3V_cor = TVector3(C_x - (DV_x - pvx),C_y - (DV_y - pvy),C_z - (DV_z - pvz))
                jet3V = TVector3(C_x,C_y,C_z)
                tester = (ev.jet_E.at(k))**2 - (ev.jet_pt.at(k)*cosh(ev.jet_eta.at(k)))**2
                if (tester < 0):
                    tester =0
                jet_m = sqrt(tester)
                jetVec_cor_temp = TLorentzVector()
                jetVec_cor = TLorentzVector()
                jetVec = TLorentzVector()
                jetVec_cor_temp.SetXYZT(C_x - (DV_x - pvx),C_y - (DV_y - pvy),C_z - (DV_z - pvz),ev.jet_E.at(k))
                jetVec_cor.SetPtEtaPhiM((sqrt((ev.jet_E.at(k))**2 - jet_m**2))/cosh(jetVec_cor_temp.Eta()),jetVec_cor_temp.Eta(),jetVec_cor_temp.Phi(),jet_m)
                # jetVec.SetXYZT(C_x,C_y,C_z,ev.jet_E.at(k))
                jetVec.SetPtEtaPhiM(ev.jet_pt.at(k),ev.jet_eta.at(k),ev.jet_phi.at(k),jet_m)
                if (jetVec_cor.DeltaR(DV,useRapidity = kTRUE) < 0.6 < jetVec.DeltaR(DV,useRapidity = kTRUE)):
                    h_ind.Fill(1)
                    improved +=1
                # print ev.jet_HadronConeExclTruthLabelID.at(k)
                if (jetVec_cor.DeltaR(DV,useRapidity = kTRUE) > 0.6 > jetVec.DeltaR(DV,useRapidity = kTRUE)):
                    h_ind.Fill(0)
                    dropped +=1
                if (ev.jet_HadronConeExclTruthLabelID.at(k) == 5):
                    h_bjet_event.Fill(ev.jet_rapidity.at(k),ev.jet_phi.at(k))
                else:
                    h_jet_event.Fill(ev.jet_rapidity.at(k),ev.jet_phi.at(k))
                    h_jet_cor_event.Fill(jetVec_cor.Rapidity(),jetVec_cor.Phi())
                if (jetVec_cor.DeltaR(DV,useRapidity = kTRUE) < minJetDR):
                    minJetDR = jetVec_cor.DeltaR(DV,useRapidity = kTRUE)
                    minJetDR_nor = jetVec_cor.DeltaR(DV)
                    minJetDR_3d = jet3V_cor.DeltaR(DV_3D-PV)
                    minJetDR_3d_uncor = jet3V.DeltaR(DV_3D-PV)
                    minJetDR_uncor = jetVec.DeltaR(DV,useRapidity = kTRUE)
                    # print "\t\tminjetDR_uncor:\t\t\t"+str(minJetDR_uncor)
                    # print "\t\tminjetDR_cor:\t\t\t"+str(minJetDR)
                    # print "\t\tminJetDR_cor no rapidity:\t"+str(minJetDR_nor)
                    # print "\t\tminjetDR_cor_3D:\t\t"+str(minJetDR_3d)
                    # print "\t\tDV_3D (eta,phi):\t\t(%.2f,%.2f)" % (DV_3D.Eta(),DV_3D.Phi())
                    # print "\t\tDV_3D-PV (eta,phi):\t\t(%.2f,%.2f)" % ((DV_3D-PV).Eta(),(DV_3D-PV).Phi())
                    jet_phi = jetVec_cor.Phi()
                    jet_eta = jetVec_cor.Eta()
                    jet_phi_uncor = ev.jet_phi.at(k)
                    jet_eta_uncor = ev.jet_eta.at(k)
                    jet_dphi = jetVec.DeltaPhi(jetVec_cor)
                    jet_diffdphi = jet3V_cor.DeltaPhi(jet3V)
                    jet_diffdeta = abs(jet3V.Eta() - jet3V_cor.Eta())
                    jet_deta = abs(jet_eta-jet_eta_uncor)
                    dminjetDR = minJetDR-minJetDR_uncor
                    if (dminjetDR < 0):
                        neg_dR = dminjetDR
                    j_x = jet3V.X()
                    j_y = jet3V.Y()
                    j_z = jet3V.Z()
                    jc_x = jet3V_cor.X()
                    jc_y = jet3V_cor.Y()
                    jc_z = jet3V_cor.Z()
                    dv_x = DV_3D.X()
                    dv_y = DV_3D.Y()
                    dv_z = DV_3D.Z()
                    jet_index = k
                    DV_index = j
                    dv_eta = ev.secVtxMedium_eta.at(DV_index)
                    dv_phi = ev.secVtxMedium_phi.at(DV_index)
                    # dv_eta = ev.truth_LLP_eta.at(DV_index)
                    # dv_phi = ev.truth_LLP_phi.at(DV_index)
                    j_pt = jetVec.Pt()
                    j_pt_cor = jetVec_cor.Pt()
                    truth_jet_ID = ev.jet_HadronConeExclTruthLabelID.at(jet_index)
                    centroid = C_R
                    jetDR = jetVec_cor.DeltaR(jetVec,useRapidity = kTRUE)
                truth_jet_ID = ev.jet_HadronConeExclTruthLabelID.at(k)
                tester = (ev.jet_E.at(k))**2 - (ev.jet_pt.at(k)*cosh(ev.jet_eta.at(k)))**2
                if (tester < 0):
                    tester =0
                jet_m = sqrt(tester)
                jet_pt_temp = (sqrt((ev.jet_E.at(k))**2 - jet_m**2))/cosh(ev.jet_eta.at(k))
                # print '\t\tJet Index:\t'+str(k)
                # print '\t\tJet uncorr (x,y,z,pt,pdgID)\t\t\t (%.1f,%.1f,%.1f,%.2f,%i)' % (C_x,C_y,C_z,jet_pt_temp,truth_jet_ID)
                # print '\t\tJet corr 4momentum (pt,eta,phi,m,y)\t\t (%.2f,%.2f,%.2f,%.2f,%.2f)' % (jetVec_cor.Pt(),jetVec_cor.Eta(),jetVec_cor.Phi(),jetVec_cor.M(),jetVec_cor.Rapidity())
                # print '\t\tJet 3D corr (pt,eta,phi,m)\t\t\t (%.2f,%.2f,%.2f,%.2f)' % (jetVec_cor.Pt(),jet3V_cor.Eta(),jet3V_cor.Phi(),jet_m)
                # print '\t\tJet uncorr real (pt,eta,phi,m,y)\t\t (%.2f,%.2f,%.2f,%.2f,%.2f)' % (ev.jet_pt.at(k),ev.jet_eta.at(k),ev.jet_phi.at(k),jet_m,ev.jet_rapidity.at(k))
                # print '\t\tJet 3D uncorr (pt,eta,phi,m)\t\t\t (%.2f,%.2f,%.2f,%.2f)' % (ev.jet_pt.at(k),jet3V.Eta(),jet3V.Phi(),jet_m)
                # print '************************'
            drawjets = True
            tindex = False
            if (truth_jet_ID == 5):
                jet_matched += 1
            for i,tid in enumerate(ev.truthVtx_ID):
                if (ev.truthVtx_closestRecoVtx_ID.at(i) == ev.secVtxMedium_ID.at(DV_index)):
                    dv_matched+=1
                    truth_index = i
                    tindex = True
            if (truth_aa == True):
                trueLLP = []
                for l,truth_LLP_pt in enumerate(ev.truth_LLP_pt):
                    truth_LLP_eta = ev.truth_LLP_eta.at(l)
                    truth_LLP_phi = ev.truth_LLP_phi.at(l)
                    truth_LLP_m = ev.truth_LLP_m.at(l)
                    truth_LLP = TLorentzVector()
                    truth_LLP.SetPtEtaPhiM(truth_LLP_pt,truth_LLP_eta,truth_LLP_phi,truth_LLP_m)
                    # print 'Truth LLP Rapidity:\t%.2f' % (truth_LLP.Rapidity())
                    trueLLP.append(truth_LLP)
                aaDR = trueLLP[0].DeltaR(trueLLP[1],useRapidity = kTRUE)
            if (minJetDR < 0.6 and dminjetDR < 0.2):
                print 'Event Number:\t'+str(ev.eventNumber)
                print 'DV Index:\t'+str(DV_index)
                print 'Jet Index:\t'+str(jet_index)
                print "DV \t\t(x,y,z,parent_pdgID):\t\t(%.1f,%.1f,%.1f,%i)" % (dv_x,dv_y,dv_z,ev.secVtxMedium_maxlinkTruth_parent_pdgId.at(DV_index))
                print "DV matched score %.2f, ID %i and (x,y,z):\t(%.1f,%.1f,%.1f)" % (ev.secVtxMedium_maxlinkTruth_score.at(DV_index),ev.secVtxMedium_maxlinkTruth_ID.at(DV_index),ev.secVtxMedium_maxlinkTruth_x.at(DV_index),ev.secVtxMedium_maxlinkTruth_y.at(DV_index),ev.secVtxMedium_maxlinkTruth_z.at(DV_index))
                if (tindex):
                    print "Truth DV score %.2f, ID %i and (x,y,z):\t(%.1f,%.1f,%.1f)" % (ev.truthVtx_maxlinkedRecoVtx_score.at(truth_index),ev.truthVtx_ID.at(truth_index),ev.truthVtx_x.at(truth_index),ev.truthVtx_y.at(truth_index),ev.truthVtx_z.at(truth_index))
                else:
                    for l,truth_DV_x in enumerate(ev.truthVtx_x):
                        truth_DV_y = ev.truthVtx_y.at(l)
                        truth_DV_z = ev.truthVtx_z.at(l)
                        truth_DV = TVector3(truth_DV_x,truth_DV_y,truth_DV_z)
                        print "Truth DV (score,ID) (%.2f,%i) position (x,y,z,parent_pdgID):\t(%.1f,%.1f,%.1f,%i)" % (ev.truthVtx_maxlinkedRecoVtx_score.at(l),ev.truthVtx_ID.at(l),truth_DV_x,truth_DV_y,truth_DV_z,ev.truthVtx_parent_pdgId.at(l))
                print "PV \t\t(x,y,z):\t (%.1f,%.1f,%.1f)" % (PV.X(),PV.Y(),PV.Z())
                print ""
                print "CentroidR Before Correction:\t%2f" % (centroid)
                print "CentroidR After Correction:\t%2f" % (sqrt(jc_x**2+jc_y**2+jc_z**2))
                print "Before Correction:"
                print "Uncor Jet \t(eta,phi,x,y,z,pt,pdgID):\t (%4f,%4f,%.1f,%.1f,%.1f,%.2f,%i)" % (jet_eta_uncor,jet_phi_uncor,j_x,j_y,j_z,j_pt,ev.jet_HadronConeExclTruthLabelID.at(jet_index))
                print ""
                print "After Correction:"
                print "Cor Jet \t(eta,phi,x,y,z,pt,pdgID):\t (%4f,%4f,%.1f,%.1f,%.1f,%.2f,%i)" % (jet_eta,jet_phi,jc_x,jc_y,jc_z,j_pt_cor,ev.jet_HadronConeExclTruthLabelID.at(jet_index))
                print "delta_minjetDR_cor:\t%.5f" % (dminjetDR)
                print ""
                print "minjetDR_cor:\t%.5f" % (minJetDR)
                print "minjetDR:\t%.5f" % (minJetDR_uncor)
                print "aaDR:\t%.5f" % (aaDR)
                print ""
                print '-----------------------------------------------'
            h_minjetDR.Fill(minJetDR)
            h_jet_phi.Fill(jet_phi)
            h_jet_eta.Fill(jet_eta)
            h_new_centroid.Fill(sqrt(jc_x**2+jc_y**2+jc_z**2))
            h_minjetDR_uncor.Fill(minJetDR_uncor)
            h_jet_phi_uncor.Fill(jet_phi_uncor)
            h_jet_eta_uncor.Fill(jet_eta_uncor)
            h_uncor_centroid.Fill(centroid)

            h_dpt.Fill(j_pt_cor-j_pt)
            h_jetDR.Fill(jetDR)
            h_lxy.Fill(sqrt(dv_x**2+dv_y**2))
            h_aaDR.Fill(aaDR)
            if (minJetDR < 0.6):
                h_dminjetDR.Fill(dminjetDR)
                h_truedminjetDR.Fill(dminjetDR)
                h_diffdminjetDR.Fill(minJetDR_3d-minJetDR_3d_uncor)
                h_truejetDR.Fill(jetDR)
                h_trueaaDR.Fill(aaDR)
                h_jet_dphi.Fill(jet_dphi)
                h_jet_deta.Fill(jet_deta)
                h_jet_diffdphi.Fill(jet_dphi)
                h_jet_diffdeta.Fill(jet_deta)
                # h_angular_event.Fill(ev.truthVtx_eta.at(truth_index),ev.truthVtx_phi.at(truth_index))
                # print 'truth dv (%.2f,%.2f)' % (ev.truthVtx_eta.at(truth_index),ev.truthVtx_phi.at(truth_index))
                # h_jet_event.Fill(jet_eta_uncor,jet_phi_uncor)
                # print 'jet dv (%.2f,%.2f)' % (jet_eta_uncor,jet_phi_uncor)
        #or you could also create a new histogram with the modified variable
        # new_centroid = centroid*2
        # h_new_centroid.Fill(new_centroid)
        hist2d = [h_angular_event,h_jet_event,h_b_event,h_jet_cor_event,h_bjet_event,h_dv_event,h_pvdv_event]
        for h in hist2d:
            h.GetXaxis().SetTitle('y')
            h.GetYaxis().SetTitle('#phi')
            if h.GetName()=='h_angular_event':
                h.SetMarkerStyle(29)
                h.SetMarkerSize(2.5)
                h.SetMarkerColorAlpha(2,0.6)
                h.Draw('P')
                drawleg(0.82,0.82,'LLP')
                drawmarker(3.4,3.25,29,2)
                # leg.AddEntry(h, 'LLP_{truth}', "p")
            if h.GetName()=='h_jet_event':
                h.SetMarkerStyle(20)
                h.SetMarkerSize(7.5)
                h.SetMarkerColorAlpha(4,0.25)
                h.Draw('P SAME')
                # leg.AddEntry(h, 'jet_{uncorr}', "p")
                drawleg(0.82,0.79,'Reco jet')
                drawmarker(3.4,2.95,20,4)
            # if h.GetName()=='h_jet_cor_event':
            #     h.SetMarkerStyle(20)
            #     h.SetMarkerSize(7.5)
            #     h.SetMarkerColorAlpha(7,0.25)
            #     h.Draw('P SAME')
            #     drawleg(0.82,0.79,'jet_{corr}')
            #     drawmarker(3.4,2.95,20,7)
                # leg.AddEntry(h, 'jet_{corr}', "p")
            if h.GetName()=='h_bjet_event':
                h.SetMarkerStyle(20)
                h.SetMarkerSize(7.5)
                h.SetMarkerColorAlpha(6,0.25)
                h.Draw('P SAME')
                drawleg(0.82,0.76,'Reco b-jet')
                drawmarker(3.4,2.65,20,6)
                # leg.AddEntry(h, 'b-jet_{uncorr}', "p")
            if h.GetName()=='h_b_event':
                h.SetMarkerStyle(20)
                h.SetMarkerSize(1.5)
                h.SetMarkerColorAlpha(3,0.65)
                h.Draw('P SAME')
                drawleg(0.82,0.73,'b-quark')
                drawmarker(3.4,2.35,20,3)
            if h.GetName()=='h_dv_event':
                h.SetMarkerStyle(5)
                h.SetMarkerSize(2.5)
                h.SetMarkerColorAlpha(1,0.6)
                h.Draw('P SAME')
                drawleg(0.82,0.70,'DV 4-mom')
                drawmarker(3.4,2.05,5,1)
                # leg.AddEntry(h, 'b_{truth}', "p") h_dv_event h_pvdv_event
            if h.GetName()=='h_pvdv_event':
                h.SetMarkerStyle(3)
                h.SetMarkerSize(2.5)
                h.SetMarkerColorAlpha(12,0.6)
                h.Draw('P SAME')
                drawleg(0.82,0.67,'PV#rightarrowDV')
                drawmarker(3.4,1.75,3,12)
        for bline in b_lines:
            line = TLine(bline[0][0],bline[0][1],bline[1][0],bline[1][1])
            line.SetLineStyle(7)
            line.SetLineColor(3)
            if (abs(bline[0][1]-bline[1][1]) < math.pi):
                line.DrawLine(bline[0][0],bline[0][1],bline[1][0],bline[1][1])
            elif (abs(bline[0][1]-bline[1][1]) >= math.pi):
                if (bline[0][1] > 0):
                    line.DrawLine(bline[0][0],bline[0][1],bline[1][0],bline[1][1]+2*math.pi)
                    line.DrawLine(bline[0][0],bline[0][1]-2*math.pi,bline[1][0],bline[1][1])
                else:
                    line.DrawLine(bline[0][0],bline[0][1]+2*math.pi,bline[1][0],bline[1][1])
                    line.DrawLine(bline[0][0],bline[0][1],bline[1][0],bline[1][1]-2*math.pi)
            print '(%.2f,%.2f)\t(%.2f,%.2f)' % (bline[0][0],bline[0][1],bline[1][0],bline[1][1])
        leg.SetBorderSize(0)
        leg.SetFillColor(0)
        leg.SetTextSize(0.035)
        leg.SetTextFont(42)
        # leg.Draw()
        draw_tex(name)
        t3 = TLatex()
        t3.SetNDC(kTRUE)
        t3.SetTextFont(42)
        t3.SetTextSize(0.028)
        t3.DrawLatex(0.16,.965,'#bf{Event} '+str(evn))
        can.Print(outDir+"/jetmedium_"+'h_angular_event_'+name+"_plots.pdf")
        can.SaveAs(outDir+"/jetmedium_"+'h_angular_event_'+name+"_"+str(evn)+".png")
        can.SaveAs(outDir+"/jetmedium_"+'h_angular_event_'+name+"_"+str(evn)+".pdf")
        can.Clear()
        leg.Clear()
        h_angular_event.Reset()
        h_jet_event.Reset()
        h_bjet_event.Reset()
        h_jet_cor_event.Reset()
        h_b_event.Reset()
        h_dv_event.Reset()
        h_pvdv_event.Reset()
    can.Print(outDir+"/jetmedium_"+'h_angular_event_'+name+"_plots.pdf]")
    del can
    del leg
    # print "Total Number of Events:\t "+str(tlen)
    # print "DV truth matched:\t " + str(dv_matched)
    # print "total truth DVs:\t " + str(total_truths)
    # print "truth DV matching efficiency:\t " + str(dv_matched/total_truths)
    # print "total truth jets matched:\t " + str(jet_matched)
    # print "total truth jets:\t " + str(total_truth_jets)
    # print "truth jet matching efficiency:\t " + str(jet_matched/total_truth_jets)
    # hist2d = [h_angular_event]
    dhists = [h_jet_dphi,h_jet_deta,h_dminjetDR,h_dpt,h_jetDR,h_ind,h_lxy]
    outputhists += [h_new_centroid, h_minjetDR, h_jet_phi, h_jet_eta]
    histlist = ['jet_CentroidR','jet_minjetDR','jet_phi','jet_eta']
    oldhist = []
    uncor_hists = [h_uncor_centroid,h_minjetDR_uncor,h_jet_phi_uncor,h_jet_eta_uncor]
    print "improved by %i" % improved
    print "dropped by %i" % dropped
    # gROOT.SetBatch(True)
    # atlasrootstyle_path = '/afs/cern.ch/work/d/dstarko/WorkArea/.local/opt/atlasrootstyle'
    # gROOT.SetMacroPath(atlasrootstyle_path)
    # gROOT.LoadMacro("AtlasStyle.C")
    # gROOT.LoadMacro("AtlasLabels.C")
    # TH1.SetDefaultSumw2(True)
    # SetAtlasStyle()
    gStyle.SetMarkerSize(0.8)
    for dhist in dhists:
        can  = TCanvas("can", "can", 0, 0, 600, 600)
        ymax = find_max([dhist])
        # if (hist.GetName() == 'h_minjetDR'):
        can.SetLogy()
        dhist.SetMaximum(20*ymax)
        # else:
        # dhist.SetMaximum(1.4*ymax)
        dhist.Draw('HIST')
        dhist.SetMarkerColor(1)
        dhist.SetLineColor(3)
        leg= TLegend(0.7, 0.73, 0.85, 0.9)
        leg.AddEntry(dhist, dhist.GetName(), "l")
        leg.SetBorderSize(0)
        leg.SetFillColor(0)
        leg.SetTextSize(0.04)
        leg.SetTextFont(42)
        leg.Draw()

        draw_tex(name)
        can.Update()
        can.SaveAs(outDir+"/jetmedium_"+dhist.GetName()+'_'+name+".png")
        can.SaveAs(outDir+"/jetmedium_"+dhist.GetName()+'_'+name+".pdf")
        del can
    dual_hists(h_dminjetDR,h_truedminjetDR)
    dual_hists(h_diffdminjetDR,h_truedminjetDR)
    dual_hists(h_jetDR,h_truejetDR)
    dual_hists(h_aaDR,h_trueaaDR)
    dual_hists(h_jet_dphi,h_jet_diffdphi)
    dual_hists(h_jet_deta,h_jet_diffdeta)
    for l,hist in enumerate(outputhists):
        can  = TCanvas("can", "can", 0, 0, 600, 600)
        pad1 = TPad("pad1","pad1",0,0.3,1,1)
        pad2 = TPad("pad2","pad2",0,0,1,0.29)
        pad1.SetBottomMargin(0.025)
        pad2.SetTopMargin(0.04)
        pad2.SetBottomMargin(0.4)

        pad1.Draw()
        pad2.Draw()
        pad1.cd()
        ymax = find_max([hist,uncor_hists[l]])
        if (hist.GetName() == 'h_minjetDR'):
            pad1.SetLogy()
            hist.SetMaximum(20*ymax)
        else:
            hist.SetMaximum(1.4*ymax)
        hist.Draw('HIST')
        uncor_hists[l].Draw('SAME HIST')
        hist.SetMarkerColor(1)
        uncor_hists[l].SetMarkerColor(3)
        uncor_hists[l].SetLineColor(3)
        leg= TLegend(0.7, 0.73, 0.85, 0.9)
        leg.AddEntry(hist, hist.GetName(), "l")
        leg.AddEntry(uncor_hists[l], uncor_hists[l].GetName(), "l")
        leg.SetBorderSize(0)
        leg.SetFillColor(0)
        leg.SetTextSize(0.04)
        leg.SetTextFont(42)
        leg.Draw()

        draw_tex(name)

        can.cd()
        pad2.cd()
        pad2.SetGridy()

        # Abs(weight_mc) + axis setup
        h2r = hist.Clone("h2r")
        h2r.SetStats(0)
        h2r.Divide(h2r,uncor_hists[l],1,1)
        h2r.SetMaximum(1.4*h2r.GetMaximum())
        if (h2r.GetMinimum() != 0):
            h2r.SetMinimum(0.6*h2r.GetMinimum())
        else:
            h2r.SetMinimum(0)
        # h2r.GetXaxis().SetRangeUser(xmin,xmax)

        h2r.GetXaxis().SetTitleSize(.16)
        h2r.GetXaxis().SetTitleOffset(1.1)
        h2r.GetXaxis().SetLabelSize(0.125)
        h2r.GetXaxis().SetLabelOffset(0.03)
        h2r.GetXaxis().SetTitle(hist.GetName())

        h2r.GetYaxis().SetNdivisions(406,1)
        h2r.GetYaxis().SetTitleSize(0.13)
        h2r.GetYaxis().SetLabelSize(.1)
        h2r.GetYaxis().SetTitleOffset(0.45)
        h2r.GetYaxis().SetTitle("Ratio")
        h2r.SetFillColorAlpha(hist.GetMarkerColor(), 0.5)
        h2r.Draw("PE3")

        line = TF1("line", "1", -1e5, 1e5)
        line.SetLineColor(51)
        line.SetLineWidth(1)
        line.Draw("SAME")

        can.cd()
        can.Update()
        can.SaveAs(outDir+"/jetmedium_"+hist.GetName()+'_'+name+".png")
        can.SaveAs(outDir+"/jetmedium_"+hist.GetName()+'_'+name+".pdf")
        del can
    rootFile.Close()
    fk.cd()
    # for h in outputhists:
    #     h.Write()
    int_jetDR = h_truejetDR.Integral()
    int_aaDR = h_trueaaDR.Integral()
    h_truejetDR.Scale(1./(int_jetDR))
    h_trueaaDR.Scale(1./(int_aaDR))
    h_truejetDR.Write()
    h_trueaaDR.Write()
    fk.Close()
    # return taaDR
infiles = {
    # 'm15_10':"/eos/atlas/atlascerngroupdisk/phys-exotics/ueh/VH4b/R21/MININTUP/ZH_Signal.2020-09-17.1.0/merged_ntuples/ZH_H125_a15a15_4b_ctau10.root",
    # 'm15_100':"/eos/atlas/atlascerngroupdisk/phys-exotics/ueh/VH4b/R21/MININTUP/ZH_Signal.2020-09-17.1.0/merged_ntuples/ZH_H125_a15a15_4b_ctau100.root",
    # 'm55_10':"/eos/atlas/atlascerngroupdisk/phys-exotics/ueh/VH4b/R21/MININTUP/ZH_Signal.2020-09-17.1.0/merged_ntuples/ZH_H125_a55a55_4b_ctau10.root",
    'm55_100':"/eos/atlas/atlascerngroupdisk/phys-exotics/ueh/VH4b/R21/MININTUP/ZH_Signal.2020-09-17.1.0/merged_ntuples/ZH_H125_a55a55_4b_ctau100.root",
    # 'm55_1000':"/eos/atlas/atlascerngroupdisk/phys-exotics/ueh/VH4b/R21/MININTUP/ZH_Signal.2020-09-17.1.0/merged_ntuples/ZH_H125_a55a55_4b_ctau1000.root"
    }
aaDR = {}
hists = []
global outDir
outDir = 'jetcomps_plots'
if not os.path.exists(outDir):
    os.makedirs(outDir)
for key in infiles.keys():
    ToArray(infiles[key], key, "test")
# gROOT.SetBatch(True)
rootFile = ROOT.TFile.Open("test.root")
for key in infiles.keys():
    aaDR[key] = rootFile.Get('h_trueaaDR_'+key)
    hists.append(aaDR[key])
can  = TCanvas("can", "can", 0, 0, 600, 600)
ymax = find_max(hists)
can.SetLogy()
leg= TLegend(0.7, 0.73, 0.85, 0.9)
for i,key in enumerate(aaDR.keys()):
    aaDR[key].SetMaximum(20*ymax)
    aaDR[key].Draw('HIST SAME')
    aaDR[key].SetMarkerColor(i+1)
    aaDR[key].SetLineColor(i+1)
    # leg= TLegend(0.7, 0.73, 0.85, 0.9)
    leg.AddEntry(aaDR[key], key, "l")
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetTextSize(0.04)
    leg.SetTextFont(42)
leg.Draw()
draw_tex(name)
can.Update()
can.SaveAs(outDir+'/jetmedium_aaDRfull.png')
can.SaveAs(outDir+"/jetmedium_aaDRfull.pdf")
# fk.Close()
'''
def drawItem(x, y, text, font = 42, color = 1, size = 0.04, align = 11):
    l = ROOT.TLatex()
    l.SetNDC() #;
    l.SetTextAlign(align)
    l.SetTextFont(font) #;
    l.SetTextSize(size)
    l.SetTextColor(color)
    l.DrawLatex(x,y,text) #;

def PlotLeaks(fname):
    AtlasPath="/eos/user/a/anmrodri/Tevatron/ATLASstyle/"
    gROOT.LoadMacro(str(AtlasPath)+'AtlasStyle.C')
    SetAtlasStyle()
    gROOT.LoadMacro(str(AtlasPath)+"AtlasUtils.C")
    gROOT.LoadMacro(str(AtlasPath)+"AtlasLabels.C")


    #f = TFile.Open(str(fname)+"/root/ABCDregions.root", "read")

    f = open(str(fname)+"/ABCDregions.txt", "r")

    canvas=TCanvas("Canvas")
    legend=TLegend(.2,.18,.4,.35)
    legend.SetBorderSize(0);
    legend.SetFillColor(0);
    legend.SetTextSize(0.03)
    graphs = []

    lines = f.readlines()[2:]
    value_array = []
    ncharge = 1

    for n, line in enumerate(lines):
        i = len(graphs)
        mass = int(line.split("\t")[3])
        charge = int(line.split("\t")[2])

        A = float(line.split("\t")[5])
        B = float(line.split("\t")[6])
        C = float(line.split("\t")[7])
        D = float(line.split("\t")[8])

        total = A+B+C+D
        value = A/total

        print value, mass, charge, total

        if charge != ncharge or (charge == 100 and mass == 4000):
            if charge == 100:
                value_array += [value]
            g=TGraph(len(mass_array))
            print value_array

            for c,l in zip(mass_array, value_array):
                g.SetPoint(mass_array.index(c),c,l)

            legend.AddEntry(g,"DY "+str(ncharge))

            g.SetMarkerStyle(20)
            g.SetMarkerSize(0.7)
            g.SetMarkerColor(colors[i])
            g.SetLineColor(colors[i])
            g.GetHistogram().SetMinimum(0)
            g.GetHistogram().SetMaximum(1)
            g.GetYaxis().SetTitle("Fraction of Signal in region A")
            g.GetXaxis().SetTitle("Mass [GeV]")

            if i == 0:
                g.Draw("ALP")
            else:
                g.Draw("LP")
            print "Charge "+str(ncharge)+" done"
            graphs.append(g)

            value_array = []
            value_array += [value]
            ncharge = charge
        else:
            value_array += [value]

    legend.Draw("same")
    print "Almost done"
    print "ATLAS Label"

    #ATLASLabel(0.4,0.26)
    drawItem(0.53,0.26,"Work in progress",42,1)
    drawItem(0.4, 0.22, "fHT > "+str(cut_fHT)+", w > "+str(cut_w) ,42, 1, 0.03)
    canvas.Update()

    print "Stuffs"
    canvas.SaveAs(str(fname)+"/Plots/Leaks_"+str(fname)+".png")
'''
